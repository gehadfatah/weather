package com.example.godaa.sunshine.service.Restfulretrofit;

import android.net.Uri;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by godaa on 10/06/2017.
 */

public class Restclient {

    public Retrofit getclient(String postalcode) {
        String format = "json";
        String units = "metric";
        int numDays = 14;
        final String  location_qyery=postalcode;

        // Construct the URL for the OpenWeatherMap query
        // Possible parameters are avaiable at OWM's forecast API page, at
        // http://openweathermap.org/API#forecast
        final String FORECAST_BASE_URL =
                "http://api.openweathermap.org/data/2.5/forecast/daily?";
        final String QUERY_PARAM = "q";
        final String FORMAT_PARAM = "mode";
        final String UNITS_PARAM = "units";
        final String DAYS_PARAM = "cnt";
        String APPID = "appid";
        String appid="aa382dc2c439e05c9cd8e5af6726b756";
        Uri builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                .appendQueryParameter(QUERY_PARAM, location_qyery)
                .appendQueryParameter(FORMAT_PARAM, format)
                .appendQueryParameter(UNITS_PARAM, units)
                .appendQueryParameter(DAYS_PARAM, Integer.toString(numDays))
                .appendQueryParameter(APPID,appid)
                .build();
        String url =builtUri.toString();
        Retrofit.Builder builder=new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(url)
                ;
        return builder.build();
    }
   /* public interface apiService{
        @GET
        Call<>
    }*/
}

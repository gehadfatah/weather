package com.example.godaa.sunshine.sync;

import android.os.AsyncTask;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

/**
 * Created by godaa on 21/06/2017.
 */

public class SunshineFirebaseJobService extends JobService {
    AsyncTask Backgroundtask;

    @Override
    public boolean onStartJob(final JobParameters job) {
        Backgroundtask=new AsyncTask() {

            @Override
            protected Object doInBackground(Object[] params) {
                SunshineSyncTask.syncWeather(getApplicationContext());

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                // well as a false value to signify that we don’t have any more work to do.
                jobFinished(job,false);
            }
        };
        Backgroundtask.execute();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        if (Backgroundtask != null) {
            Backgroundtask.cancel(true);
        }
        //Yes please, we’d like to be rescheduled to finish that work that we were doing when you so rudely interrupted us
        return true;
    }
}

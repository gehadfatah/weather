package com.example.godaa.sunshine.sync;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.format.DateUtils;

import com.example.godaa.sunshine.DetailActivity;
import com.example.godaa.sunshine.MainActivity;
import com.example.godaa.sunshine.R;
import com.example.godaa.sunshine.Utility;
import com.example.godaa.sunshine.data.WeatherContract;

import java.util.Date;

import static android.text.format.DateUtils.DAY_IN_MILLIS;

/**
 * Created by godaa on 22/06/2017.
 */

public class NotificationsUtils {
    public static final String[] WEATHER_NOTIFICATION_PROJECTION = {
            WeatherContract.WeatherEntry.COLUMN_WEATHER_ID,
            WeatherContract.WeatherEntry.COLUMN_MAX_TEMP,
            WeatherContract.WeatherEntry.COLUMN_MIN_TEMP,
    };
    public static final int INDEX_WEATHER_ID = 0;
    public static final int INDEX_MAX_TEMP = 1;
    public static final int INDEX_MIN_TEMP = 2;
    public static final int INDEX_SHORT_DESC=3;
    private static final int WEATHER_NOTIFICATION_ID = 3004;
    public static void Notify_weather(Context context) {
        //if you check notfication check box or not
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Boolean displaynotification = preferences.getBoolean(context.getString(R.string.pref_enable_notification_key),
                Boolean.parseBoolean(context.getString(R.string.pref_enable_notification_default)));
        if (displaynotification) {


            // Checking the last update and notify if it's the first
            String lastNotificationKey = context.getString(R.string.pref_last_notification);
            long lastSync = preferences.getLong(lastNotificationKey, 0);
//if you un update yourself from day or more
            if (System.currentTimeMillis() - lastSync >= DateUtils.DAY_IN_MILLIS) {
                // Last sync was more than 1 day ago, let's send a notification
                String locationQuery = Utility.getPreferredLocation(context);

                Uri weatherUri = WeatherContract.WeatherEntry
                        .buildWeatherLocationWithDate(
                                locationQuery, WeatherContract.getDbDateString(new Date()));
//                Uri todaysWeatherUri = WeatherContract.WeatherEntry
//                        .buildWeatherUriWithDate(WeatherContract.normalizeDate(System.currentTimeMillis()));
                // We'll query our contentProvider, as always //for one row
                Cursor cursor =  context.getContentResolver().query(
                        weatherUri,
                        WEATHER_NOTIFICATION_PROJECTION,
                        null,
                        null,
                        null
                );

                if (cursor.moveToFirst()) {
                    int weatherId = cursor.getInt(INDEX_WEATHER_ID);
                    double maxTemp = cursor.getDouble(INDEX_MAX_TEMP);
                    double minTemp = cursor.getDouble(INDEX_MIN_TEMP);
                    String shortDesc = cursor.getString(INDEX_SHORT_DESC);
                    int iconId = Utility.getIconResourceForWeatherCondition(weatherId);
                    String title = context.getString(R.string.app_name);

                    // Define the text fo the forecast
                    String contentText = String.format(
                            context.getString(R.string.format_notification),
                            shortDesc,
                            Utility.formatTemperature(maxTemp, Utility.isMetric(context)),
                            Utility.formatTemperature(minTemp, Utility.isMetric(context))
                    );

                    // Build our notification using NotificationCompat.builder
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(iconId)
                                    .setContentTitle(title)
                                    .setContentText(contentText);


                    // Make something interesting happen when the user clicks on the notificaiton.
                    // In this case, opening the app is sufficient
                    Intent resultIntent = new Intent(context, DetailActivity.class);
                  //  resultIntent.setData(todaysWeatherUri);
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addNextIntentWithParentStack(resultIntent);
                    PendingIntent resultPendingIntent =
                            stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);

                    NotificationManager mNotificationManager =
                            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    // mID allows you to update the notification later on
                    mNotificationManager.notify(WEATHER_NOTIFICATION_ID, mBuilder.build());

                    // Refreshing last sync
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putLong(lastNotificationKey, System.currentTimeMillis());
                    editor.commit();
                }

            }
        }
    }
}

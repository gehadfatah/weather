package com.example.godaa.sunshine.sync;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;

import com.example.godaa.sunshine.data.WeatherContract;
import com.example.godaa.sunshine.service.CustomJSONObjectRequest;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.Trigger;

import java.sql.Driver;
import java.util.concurrent.TimeUnit;

/**
 * Created by godaa on 21/06/2017.
 */

public class SunshineSyncUtils {
   static boolean sintailized;
    static  String SUNSHINE_SYNC_TAG = "sync_sunshine";
  static   int Time_sync = 3;
  static   int time_in_second = (int) TimeUnit.HOURS.toSeconds(Time_sync);
   static int time_sync_flex=time_in_second/3;
    public static void intailize(final Context context) {
//if(sintailized) return;
        if (!sintailized) {
            //startImmediateSync();
            sintailized=true;
            schelde_sync_dispatcher(context);
           /* Thread thread=new Thread(new Runnable() {
                @Override
                public void run() {

                }
            });
            thread.start();*/
            AsyncTask asyncTask=new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] params) {
                    Cursor cursor=context.getContentResolver().query(WeatherContract.WeatherEntry.CONTENT_URI,
                            new String[] {WeatherContract.WeatherEntry._ID},
                            null,
                            null,
                            null,
                            null
                    );

                    return cursor.getCount();
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
                  if (o==0||o==null) startImmediateSync(context);

                }

            };
            asyncTask.execute();

        }

    }

    public static void schelde_sync_dispatcher(Context context) {
        com.firebase.jobdispatcher.Driver driver = new GooglePlayDriver(context);
        FirebaseJobDispatcher firebaseJobDispatcher = new FirebaseJobDispatcher(driver);
        Job job = firebaseJobDispatcher.newJobBuilder()
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .setService(SunshineFirebaseJobService.class)
                .setLifetime(Lifetime.FOREVER)
                .setTag(SUNSHINE_SYNC_TAG)
                .setRecurring(true)
                .setReplaceCurrent(true)
                .setTrigger(Trigger.executionWindow(time_in_second, time_in_second + time_sync_flex))
                .build();
        firebaseJobDispatcher.schedule(job);

    }
    public static void startImmediateSync(Context context) {
        Intent intentToSyncImmediately = new Intent(context, SunshineSyncIntentService.class);
        context.startService(intentToSyncImmediately);


    }
}

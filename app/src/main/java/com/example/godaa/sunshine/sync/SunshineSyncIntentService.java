package com.example.godaa.sunshine.sync;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

/**
 * Created by godaa on 21/06/2017.
 */

public class SunshineSyncIntentService extends IntentService {


    public SunshineSyncIntentService() {
        super("SunshineSyncIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
      //Now that we have a way to sync the weather and a way to handle backgrounding that sync,
        SunshineSyncTask.syncWeather(this);
    }
}
